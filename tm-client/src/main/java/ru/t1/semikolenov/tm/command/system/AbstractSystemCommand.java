package ru.t1.semikolenov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
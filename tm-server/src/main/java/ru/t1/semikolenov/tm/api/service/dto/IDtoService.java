package ru.t1.semikolenov.tm.api.service.dto;

import ru.t1.semikolenov.tm.api.repository.dto.IDtoRepository;
import ru.t1.semikolenov.tm.dto.model.AbstractModelDTO;

public interface IDtoService<M extends AbstractModelDTO> extends IDtoRepository<M> {
}


package ru.t1.semikolenov.tm.api.service.model;

import ru.t1.semikolenov.tm.api.repository.model.IRepository;
import ru.t1.semikolenov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
